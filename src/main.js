import Vue from "vue";
import App from "./App.vue";
import axios from "axios";
import VueRouter from "vue-router";

import { routes } from "./routes";

axios.defaults.baseURL = "https://vue-test-gns.firebaseio.com/";

Vue.use(VueRouter);

Vue.filter("currency", value => {
  return "$" + value.toLocaleString();
});

import store from "./store/store";

const router = new VueRouter({
  mode: "history",
  routes,
});

new Vue({
  el: "#app",
  router,
  store,
  render: h => h(App),
});

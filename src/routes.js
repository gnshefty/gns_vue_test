import Home from "./pages/Home.vue";
import DetailRow from "./pages/DetailRow.vue";

export const routes = [
  {
    path: "/",
    component: Home,
  },
  {
    path: "/:name",
    component: DetailRow,
    name: "Details",
  },
];
